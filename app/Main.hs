{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Main (main) where

import qualified Raylib.Core as RL_C
import qualified Raylib.Core.Text as RL_CT
import qualified Raylib.Util as RL_U

import Control.Monad.Trans.RWS (RWST (..))
import qualified Data.List as L
import qualified Data.Vector as V

import qualified Data.Map.Strict as M
import Term.Core
import Term.Prelude
import Term.Types

initWindow :: IO RL_U.WindowResources
initWindow = RL_C.initWindow 2000 1000 "terminal-hs"

-- NOTE: Map id App
-- App supports: events(could have keyboard and seperate for system?), change, display
stepTerm ::
    (Monoid w) =>
    IO events ->
    (events -> RWST e w appData (MaybeT IO) (Map Int events)) ->
    ReaderT (e, appData) IO w ->
    RWST e w appData (MaybeT IO) (Map Int events)
stepTerm events change display = do
    env <- ask
    es <- liftIO events
    newEvents <- change es
    void $ liftIO . runReaderT display . (env,) =<< get
    pure newEvents

-- NOTE: Custom RWST for ergonomics
-- runTerm?
-- try using custom data structure for internals of the Shell Map
-- rename data types
termLoop ::
    (Monoid events) =>
    RWST
        env
        ()
        (Int, Map Int (App env events appData), Map Int events)
        (MaybeT IO)
        ()
termLoop = do
    env <- ask

    whenM (liftIO $ RL_C.isKeyDown KeyLeftControl) $ do
        let ks = [KeyOne .. KeyNine]
        num <-
            liftIO
                $ L.lookup True
                . flip zip ks
                <$> traverse RL_C.isKeyPressed ks
        case num of
            Nothing -> pure ()
            Just k -> do
                let newFocus = fromEnum k - fromEnum KeyOne + 1
                smap <- gets snd3
                when (newFocus `M.member` smap)
                    $ modify (first3 $ const newFocus)

    modifyM $ \(focus, smap, _extraEvents) -> do
        lift
            . fmap
                ( (\(a, e) -> (focus, a, e))
                    . bimap M.fromList fold
                    . unzip
                    . M.elems
                )
            . sequenceA
            . M.mapWithKey
                ( \sid app ->
                    ( \(extraEvents', sdata', _) ->
                        ((sid, app{appData = sdata'}), extraEvents')
                    )
                        <$> runRWST
                            ( if focus == sid
                                then stepTerm (appEvents app) (appChange app) (appDisplay app)
                                else stepTerm (pure mempty) (appChange app) (pure mempty)
                            )
                            env
                            (appData app)
                )
            $ smap

    termLoop

untilJust :: (Monad m) => m (Maybe b) -> m b
untilJust m = m >>= maybe (untilJust m) pure

defaultApps :: (Ord k, Num k, Enum k) => Map k ShellApp
defaultApps =
    M.fromList
        . zip [1 ..]
        $ [ App shellEvents shellChange shellDisplay
                $ defaultShellData defaultOutputText 10 (toEnum 0)
          , App shellEvents notifyChange shellDisplay
                $ defaultShellData defaultOutputText 10 (toEnum 5)
          ]
  where
    defaultShellData = ShellData ":" "|> " mempty
    defaultOutputText = V.fromList $ lines "lmao\ntest\nhuh\nsus\nomg\nbruh\nfrfr"

main :: IO ()
main = do
    bracket initWindow RL_C.closeWindow $ \window -> do
        void $ untilJust (bool Nothing (pure ()) <$> RL_C.isWindowReady)
        RL_C.setTargetFPS 60
        RL_C.setConfigFlags [WindowResizable]

        firacodefont <-
            RL_CT.loadFont
                "assets/fonts/FiraCode/FiraCodeNerdFont-Medium.ttf"
                window
        void $ untilJust (bool Nothing (pure ()) <$> RL_CT.isFontReady firacodefont)

        env <-
            TermEnv firacodefont 0
                <$> RL_C.getScreenWidth
                <*> RL_C.getScreenHeight
        void
            . runMaybeT
            $ runRWST termLoop env (1, defaultApps, mempty)
