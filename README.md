- allow for sending events to and running/ticking apps that are not focused
    - need to split events to different apps
- Terminal: Map Event AppId -> RWST TermInfo () (NEZipper (AppId, App)) (MaybeT IO) ()
- App/Shell: [Event] -> RWST TermInfo () App (MaybeT IO) ()
- Split events across apps based on their ids

[Event] -> App/Command:

[Event] -> Shell/Interpreter:
Command [Params]
Command: alpha * [alphaNum]
Params:
    [ "\" | alphaNum | space | special"
    | '\' | alphaNum | space | special'
    | [alphaNum | special]
    ]
either displayError ($ params) $ M.lookup Command commandToFunction

Refactor:
- Alias the readers and rwsts
    - create specific functions like asksEnv
- Events: ActionCodes, Keysyms
- Make a param for (traverse [handlevents <.> pollEvents :: m Event])
    - and give a list of possible event creators
- Terminal handles/parses inputs (either to app as command, or to terminal as action)
    - handle :: Event -> Writer [a] [b], handles :: [Event] -> Writer [a] [b]
    - or just a handles :: [Event] -> ([a], [b])
        - might be easier if the events need to affect other events?
    - and passes them to an "appLoop" that handles them
- Extract AppInfo/App from Term, into ShellInfo/Shell which is seperate
    - appLoop no longer loops, AppState -> ExitCode(AppState), so it can be run selectively
    - Have a Zipper of Applications/appLoops(typeclass?)
- Extract defaults
- Display typeclass
- Unify the modifications of cursor/inputline
- Keymaps for inputs
