module Parser.List (
    item,
    eof,
    sat,
    alpha,
    digit,
    single,
    space,
    list,
    spaces,
    spaces1,
) where

import Parser.Core (ParserT (..), mkParserT)

import Data.Char (isAlpha, isDigit)
import Relude

-- Prim
item :: (Alternative m, Monad m) => ParserT String [a] m a
item = mkParserT $ \case
    [] -> empty
    x : xs -> pure (x, xs)

eof :: (Alternative m, Monad m) => ParserT String [a] m ()
eof = mkParserT $ \case
    [] -> pure ((), [])
    _ -> empty

sat :: (MonadPlus m) => (a -> Bool) -> ParserT String [a] m a
sat p = mfilter p item

single :: (MonadPlus m, Eq a) => a -> ParserT String [a] m a
single c = sat (c ==)

list :: (MonadPlus m, Eq a) => [a] -> ParserT String [a] m [a]
list = traverse single

-- Char Prim
alpha :: (MonadPlus m) => ParserT String [Char] m Char
alpha = sat isAlpha

digit :: (MonadPlus m) => ParserT String [Char] m Char
digit = sat isDigit

space :: (MonadPlus m) => ParserT String [Char] m Char
space = single ' '

spaces :: (MonadPlus m) => ParserT String [Char] m String
spaces = many space

spaces1 :: (MonadPlus m) => ParserT String [Char] m String
spaces1 = some space
