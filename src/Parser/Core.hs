module Parser.Core (
    ParserT (..),
    Parser,
    runParser,
    mkParser,
    runParserT,
    mkParserT,
    label,
) where

import Relude

newtype ParserT e s m a
    = ParserT
    { unParserT :: StateT s (ExceptT e m) a
    }
    deriving
        ( Functor
        , Applicative
        , Monad
        , Alternative
        , MonadPlus
        , MonadFail
        , MonadIO
        )

type Parser e s a = ParserT e s Identity a

mkParser :: a -> Parser e s a
mkParser x = ParserT $ StateT $ \s -> pure (x, s)

runParser :: ParserT e s Identity a -> s -> Either e (a, s)
runParser = fmap runIdentity . runParserT

mkParserT :: (s -> ExceptT e m (a, s)) -> ParserT e s m a
mkParserT = ParserT . StateT

runParserT :: ParserT e s m a -> s -> m (Either e (a, s))
runParserT = fmap runExceptT . runStateT . unParserT

label :: (MonadFail m, Monoid e) => String -> ParserT e s m a -> ParserT e s m a
label = flip (<|>) . fail

instance (Monad m, Semigroup a) => Semigroup (ParserT e s m a) where
    (<>) = liftA2 (<>)

instance (Monad m, Monoid a) => Monoid (ParserT e s m a) where
    mempty = mkParserT $ \s -> pure (mempty, s)
