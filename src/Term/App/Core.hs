module Term.App.Core (
    App (..),
    ShellApp,
    getTermText,
    lineDisplay,
    cursorDisplay,
    inputline,
    scrollChange,
    deleteChange,
    exitChange,
    handleTextInputs,
    getNewInput,
    sendCommand,
    dir2lzfunc,
    getHorizontalInputs,
    isKeyPressedOrRepeat,
    getUnicodeInputs,
) where

import qualified Raylib.Core as RL_C
import qualified Raylib.Core.Shapes as RL_CS
import qualified Raylib.Core.Text as RL_CT
import qualified Raylib.Util.Colors as RL_UC

import Control.Monad.Trans.RWS (RWST (..))
import qualified Data.List.Zipper as LZ
import qualified Data.Text as T
import qualified Data.Vector as V

import Term.Font
import Term.Prelude
import Term.Types

data App env events appData
    = App
    { appEvents :: IO events
    , appChange :: events -> RWST env () appData (MaybeT IO) (Map Int events)
    , appDisplay :: ReaderT (env, appData) IO ()
    , appData :: appData
    }

type ShellApp = App TermEnv ShellEvents ShellData

getTermText :: ReaderT (TermEnv, ShellData) IO [Text]
getTermText = do
    sdata <- asks snd
    (llen, lcount) <-
        liftReader
            . bitraverse getLineLenFW getLineCountFH
            =<< getFontDimensionsInt
    let breakIfLonger s =
            if' (T.null s) [""]
                $ whenHead ((llen ==) . T.length) ("" :)
                . reverse
                $ T.chunksOf llen s
        whenHead p f xs =
            fromMaybe xs
                $ (\x -> guard (p x) $> f xs)
                =<< safeHead xs
    pure
        . take (lcount + 1)
        $ breakIfLonger
        =<< if outputScroll sdata == 0
            then T.pack (LZ.toList $ inputline sdata) : V.toList (output sdata)
            else
                T.pack (scrollPreInput sdata)
                    : ( V.toList
                            . V.drop (fromEnum (outputScroll sdata) - 1)
                            $ output sdata
                      )

lineDisplay :: TermEnv -> (Float, Text) -> IO ()
lineDisplay env (h, line) =
    RL_CT.drawTextEx
        (font env)
        (T.unpack line)
        (Vector2 0 h)
        (fontSize $ font env)
        (fontSpacing env)
        RL_UC.white

cursorDisplay :: (Enum a) => Int -> Int -> a -> a -> Color -> IO ()
cursorDisplay cx cy fw fh =
    RL_CS.drawRectangleLines
        cx
        cy
        (fromEnum fw)
        (fromEnum fh)

inputline :: ShellData -> LZ.Zipper Char
inputline = LZ.left . flip (<>) . reverse <$> preInput <*> input

scrollChange :: (Monoid w) => RWST e w ShellData (MaybeT IO) ()
scrollChange =
    whenM (liftIO $ RL_C.isKeyDown KeyLeftControl) $ do
        whenM (liftIO $ isKeyPressedOrRepeat KeyJ) $ do
            modify $ \sdata -> do
                maybe sdata (\x -> sdata{outputScroll = x})
                    $ minusNaturalMaybe (outputScroll sdata) 1
        whenM (liftIO $ isKeyPressedOrRepeat KeyK) $ do
            modify $ \sdata -> do
                sdata{outputScroll = outputScroll sdata + 1}

deleteChange :: (Monoid w) => RWST e w ShellData (MaybeT IO) ()
deleteChange =
    whenM (liftIO $ isKeyPressedOrRepeat KeyBackspace)
        $ modify
        $ \sdata -> sdata{input = LZ.left tryTail $ input sdata}

exitChange :: (MonadIO m, Alternative m) => m ()
exitChange =
    whenM (liftIO $ RL_C.isKeyPressed KeyEscape) empty

handleTextInputs ::
    (Monoid w) =>
    (String -> c) ->
    (c -> RWST e w ShellData (MaybeT IO) [String]) ->
    (ShellData -> String -> V.Vector Text -> V.Vector Text) ->
    ShellEvents ->
    RWST e w ShellData (MaybeT IO) ()
handleTextInputs parse run addInput events = do
    whenM (gets $ (0 ==) . outputScroll)
        $ modifyM
        $ \sdata -> do
            let newInput = getNewInput events $ input sdata
            if sentCommand events
                then sendCommand parse run newInput addInput sdata
                else pure $ sdata{input = newInput, output = output sdata}

getNewInput :: ShellEvents -> LZ.Zipper Char -> LZ.Zipper Char
getNewInput events =
    dir2lzfunc (horizontalDirection events)
        . LZ.addLefts (textInput events)

sendCommand ::
    (Monad m) =>
    ([Char] -> b) ->
    (b -> m [String]) ->
    LZ.Zipper Char ->
    (ShellData -> String -> V.Vector Text -> V.Vector Text) ->
    ShellData ->
    m ShellData
sendCommand parse run newInput addInput sdata = do
    let newInputList = LZ.toList newInput
    result <- run . parse $ newInputList
    pure
        sdata
            { input = mempty
            , output =
                V.take
                    (outputLimit sdata)
                    ( V.fromList (T.pack <$> reverse result)
                        <> addInput sdata newInputList (output sdata)
                    )
            }

dir2lzfunc :: Dir1D -> LZ.Zipper a -> LZ.Zipper a
dir2lzfunc = \case
    Zero1 -> id
    Forward1 -> LZ.tryShiftRight
    Backward1 -> LZ.tryShiftLeft

getHorizontalInputs :: IO Dir1D
getHorizontalInputs = do
    fold
        [ bool Zero1 Forward1 <$> isKeyPressedOrRepeat KeyRight
        , bool Zero1 Backward1 <$> isKeyPressedOrRepeat KeyLeft
        ]

isKeyPressedOrRepeat :: KeyboardKey -> IO Bool
isKeyPressedOrRepeat = RL_C.isKeyPressed <<||>> RL_C.isKeyPressedRepeat

getUnicodeInputs :: IO [Char]
getUnicodeInputs = do
    c <- RL_C.getCharPressed
    if c == 0
        then pure []
        else (toEnum c :) <$> getUnicodeInputs
