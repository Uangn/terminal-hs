module Term.Font (
    fontSize,
    getFontDimensions,
    getFontDimensionsInt,
    getFontWidthIO,
    getFontHeightIO,
    getLineLenIO,
    getLineCountIO,
    getLineLenFW,
    getLineCountFH,
) where

import qualified Raylib.Core.Text as RL_CT

import Term.Prelude
import Term.Types

fontSize :: (Enum a) => Font -> a
fontSize = toEnum . font'baseSize

getFontDimensions :: ReaderT (TermEnv, ShellData) IO (Float, Float)
getFontDimensions = do
    env <- asks fst
    liftIO
        $ (\(Vector2 x y) -> (x, y))
        <$> RL_CT.measureTextEx
            (font env)
            "a"
            (fontSize $ font env)
            (fontSpacing env)

getFontDimensionsInt :: ReaderT (TermEnv, ShellData) IO (Int, Int)
getFontDimensionsInt = bimapBoth fromEnum <$> getFontDimensions

getFontWidthIO :: ReaderT (TermEnv, ShellData) IO Int
getFontWidthIO = fst <$> getFontDimensionsInt

getFontHeightIO :: ReaderT (TermEnv, ShellData) IO Int
getFontHeightIO = snd <$> getFontDimensionsInt

getLineLenIO :: ReaderT (TermEnv, ShellData) IO Int
getLineLenIO = liftReader . getLineLenFW =<< getFontWidthIO

getLineCountIO :: ReaderT (TermEnv, ShellData) IO Int
getLineCountIO = liftReader . getLineCountFH =<< getFontHeightIO

getLineLenFW :: (HasCallStack) => Int -> Reader (TermEnv, ShellData) Int
getLineLenFW 0 = error "Error: Calulated font size is 0"
getLineLenFW fw = reader $ fst >>> \env -> windowWidth env `div` fw

getLineCountFH :: (HasCallStack) => Int -> Reader (TermEnv, ShellData) Int
getLineCountFH 0 = error "Error: Calulated font size is 0"
getLineCountFH fh = reader $ fst >>> \env -> windowHeight env `div` fh
