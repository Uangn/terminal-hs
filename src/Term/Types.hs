module Term.Types (
    module Term.Raylib.Types,
    Dir1D (..),
    Dir2D (..),
    TermEnv (..),
    ShellEvents (..),
    ShellData (..),
    ShellCommand (..),
    NotifyCommand (..),
) where

import qualified Data.List.Zipper as LZ
import qualified Data.Vector as V

import Term.Prelude
import Term.Raylib.Types

data ShellEvents
    = ShellEvents
    { textInput :: [Char]
    , horizontalDirection :: Dir1D
    , sentCommand :: Bool
    }
    deriving (Show)

instance Semigroup ShellEvents where
    (ShellEvents t1 hd1 ep1) <> (ShellEvents t2 hd2 ep2) =
        ShellEvents (t1 <> t2) (hd1 <> hd2) (ep1 || ep2)

instance Monoid ShellEvents where
    mempty = ShellEvents mempty mempty False

data ShellData
    = ShellData
    { scrollPreInput :: String
    , preInput :: String
    , input :: LZ.Zipper Char
    , output :: V.Vector Text
    , outputLimit :: Int
    , outputScroll :: Natural
    }
    deriving (Show)

data NotifyCommand
    = NotifyLine Natural
    | NotifyLineRange Natural Natural
    | NotifyError [String]
    | NotifyExit
    deriving (Show)

data ShellCommand
    = ShellEcho [String]
    | ShellHelp
    | ShellError [String]
    | ShellExit
    deriving (Show)

data TermEnv
    = TermEnv
    { font :: Font
    , fontSpacing :: Float
    , windowWidth :: Int
    , windowHeight :: Int
    }

data Dir1D
    = Zero1
    | Forward1
    | Backward1
    deriving (Show, Read, Eq)

instance Semigroup Dir1D where
    Zero1 <> d = d
    d <> Zero1 = d
    Forward1 <> Backward1 = Zero1
    Backward1 <> Forward1 = Zero1
    d <> _ = d

instance Monoid Dir1D where
    mempty = Zero1

data Dir2D
    = Zero2
    | Forward2
    | Backward2
    | Up2
    | Down2
    deriving (Show, Read, Eq)
