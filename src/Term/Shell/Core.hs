module Term.Shell.Core (
    shellEvents,
    shellChange,
    shellDisplay,
) where

import qualified Raylib.Core as RL_C
import qualified Raylib.Util.Colors as RL_UC

import Control.Monad.Trans.RWS (RWST (..))
import qualified Data.List as L
import qualified Data.List.Zipper as LZ
import qualified Data.Text as T
import qualified Data.Vector as V

import Parser.Core
import Term.Shell.Parser

import qualified Data.Map.Strict as M
import Term.App.Core
import Term.Font
import Term.Prelude
import Term.Types

cursorPosition :: ReaderT (TermEnv, ShellData) IO (Int, Int)
cursorPosition = do
    env <- asks fst
    (fw, fh) <- getFontDimensionsInt
    lineLenLimit <- liftReader $ getLineLenFW fw
    asks
        $ bimap
            (fw *)
            (subtract fh . (windowHeight env -) . (fh *))
        . getCursorCoords lineLenLimit
        . inputline
        . snd
  where
    getCursorCoords limit (LZ.Zipper l r) = (nl `mod` limit, height)
      where
        (nl, nr) = bimapBoth length (l, r)
        height = ((nl - ((nl `div` limit) * limit)) + nr) `div` limit

shellDisplay :: ReaderT (TermEnv, ShellData) IO ()
shellDisplay = do
    env <- asks fst
    (fw, fh) <- getFontDimensions
    (cx, cy) <- cursorPosition
    termText <- getTermText
    scrolled <- asks $ (0 /=) . outputScroll . snd
    liftIO $ bracket_ RL_C.beginDrawing RL_C.endDrawing $ do
        RL_C.clearBackground RL_UC.black
        forM_
            ( zip
                ((toEnum (windowHeight env) -) <$> [fh, 2 * fh ..])
                termText
            )
            $ lineDisplay env
        unless scrolled
            $ cursorDisplay cx cy fw fh RL_UC.white

shellRunCommand :: (Alternative m) => ShellCommand -> m [String]
shellRunCommand = \case
    ShellExit -> empty
    ShellError s -> pure s
    ShellEcho s -> pure s
    ShellHelp ->
        pure
            [ "This is the help command"
            , "It helps you"
            ]

shellParseCommand :: String -> ShellCommand
shellParseCommand =
    either (ShellError . L.lines) fst
        . fromMaybe (Left "Error: undefined error")
        . runParserT shellCommand

shellChange :: (Monoid w) => ShellEvents -> RWST e w ShellData (MaybeT IO) (Map Int ShellEvents)
shellChange events = do
    scrollChange
    deleteChange
    exitChange
    handleTextInputs shellParseCommand shellRunCommand shellAddInput events
    pure M.empty

shellAddInput :: ShellData -> String -> V.Vector Text -> V.Vector Text
shellAddInput sdata newInputList =
    V.cons (T.pack $ preInput sdata <> newInputList)

shellEvents :: IO ShellEvents
shellEvents = do
    ShellEvents
        <$> getUnicodeInputs
        <*> getHorizontalInputs
        <*> isKeyPressedOrRepeat KeyEnter
