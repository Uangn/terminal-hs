module Term.Shell.Parser (
    shellCommand,
) where

import Parser.Core (ParserT (..), label)
import Parser.List

import Term.Prelude
import Term.Types

shellCommand :: (MonadPlus m, MonadFail m) => ParserT String String m ShellCommand
shellCommand = do
    join (label "Error: Command not found" $ asum commands)
        <* label "Error: Command not parsed" (spaces *> eof)
  where
    commands =
        (\(ident, rest) -> list ident $> rest)
            <$> [ ("echo", echo)
                , ("help", help)
                , ("exit", exit)
                , ("", pure $ ShellEcho [])
                ]
    arguments = some space *> argument
    echo =
        (\args -> ShellEcho $ if null args then [""] else args)
            <$> many arguments
    help = pure ShellHelp
    exit = pure ShellExit

argument :: (MonadPlus m) => ParserT String String m String
argument =
    concat <$> some (dquoted <|> squoted <|> validStr)
  where
    squote = single '\''
    squoted = squote *> some (validChar <|> dquote <|> space) <* squote

    dquote = single '\"'
    dquoted = dquote *> some (validChar <|> squote <|> space) <* dquote

    escaped = list "\\" *> item
    validChar = escaped <|> alpha <|> digit
    validStr = some validChar
