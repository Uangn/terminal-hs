module Term.Prelude (
    module Relude,
    module Relude.Extra.Tuple,
    module Relude.Extra.Bifunctor,
    module Control.Exception,
    module Control.Arrow,
    module Data.Composition,
    module Control.Monad,
    module Control.Monad.IO.Class,
    module Foreign.C.Types,
    module Data.Word,
    module Data.Char,
    module Numeric.Natural,
    (<.>),
    (<&&>),
    (<||>),
    (<<&&>>),
    (<<||>>),
    if',
    liftState,
    liftReader,
    orElseList,
    imapMList,
    imapMList_,
    iforMList,
    iforMList_,
    padToWith,
    padTo,
    chunkedLines,
    pairwise2,
    safeHead,
    safeTail,
    tryTail,
    iterateMN,
    nTimes,
    when',
    whenM',
    modifyM,
    fst3,
    snd3,
    trd3,
    first3,
    second3,
    third3,
    uncurry3,
    curry3,
    maybeToEnum,
) where

import Control.Arrow (Arrow ((&&&), (***)))
import Control.Exception (bracket, bracket_)
import Control.Monad (unless, void, when, (<=<), (>=>))
import Control.Monad.IO.Class (MonadIO (liftIO))
import Data.Char (isDigit, toUpper)
import Data.Composition ((.*))
import Numeric.Natural (Natural, minusNaturalMaybe)

import Data.Word (Word8)
import Foreign.C.Types

import qualified Data.Text as T

import Relude
import Relude.Extra.Bifunctor
import Relude.Extra.Tuple

infixr 9 <.>
(<.>) :: (Functor f) => (a1 -> b) -> (a2 -> f a1) -> a2 -> f b
f <.> g = fmap f . g

infixr 3 <&&>
(<&&>) :: (Applicative f) => f Bool -> f Bool -> f Bool
(<&&>) = liftA2 (&&)

infixr 3 <||>
(<||>) :: (Applicative f) => f Bool -> f Bool -> f Bool
(<||>) = liftA2 (||)

infixr 3 <<&&>>
(<<&&>>) :: (Applicative f, Applicative g) => f (g Bool) -> f (g Bool) -> f (g Bool)
(<<&&>>) = liftA2 (<&&>)

infixr 3 <<||>>
(<<||>>) :: (Applicative f, Applicative g) => f (g Bool) -> f (g Bool) -> f (g Bool)
(<<||>>) = liftA2 (<||>)

if' :: Bool -> p -> p -> p
if' b t f = if b then t else f

liftState :: (Applicative f) => State s a -> StateT s f a
liftState s = StateT $ pure . runState s

liftReader :: (Applicative f) => Reader r a -> ReaderT r f a
liftReader r = ReaderT $ pure . runReader r

orElseList :: [a] -> [a] -> [a]
orElseList [] ys = ys
orElseList xs _ = xs

imapMList :: (Applicative m) => (Int -> a -> m b) -> [a] -> m [b]
imapMList = go 0
  where
    go _ _ [] = pure []
    go !i f (x : xs) = liftA2 (:) (f i x) $ go (i + 1) f xs

iforMList :: (Applicative m) => [a] -> (Int -> a -> m b) -> m [b]
iforMList = flip imapMList

imapMList_ :: (Applicative m) => (Int -> a -> m b) -> [a] -> m ()
imapMList_ = go 0
  where
    go _ _ [] = pure ()
    go !i f (x : xs) = f i x *> go (i + 1) f xs

iforMList_ :: (Applicative m) => [a] -> (Int -> a -> m b) -> m ()
iforMList_ = flip imapMList_

padToWith :: Int -> Text -> Text -> Text
padToWith n p s = s <> T.replicate (n - T.length s) p

padTo :: Int -> Text -> Text
padTo n = padToWith n " "

chunkedLines :: Int -> Text -> [Text]
chunkedLines n = lines >=> T.chunksOf n

pairwise2 :: (Bifunctor p) => (a -> b -> e) -> (c -> d -> f) -> (a, c) -> p b d -> p e f
pairwise2 g h = uncurry bimap . bimap g h

safeHead :: [a] -> Maybe a
safeHead = head <.> nonEmpty

safeTail :: [a] -> Maybe (a, [a])
safeTail = nonEmpty >=> \(x :| xs) -> pure (x, xs)

tryTail :: [a] -> [a]
tryTail = maybe [] tail . nonEmpty

iterateMN :: (Monad m) => Int -> (a -> m a) -> a -> m a
iterateMN !n g x
    | n <= 0 = return x
    | otherwise = g x >>= iterateMN (n - 1) g

nTimes :: Int -> (a -> a) -> a -> a
nTimes !n f x
    | n <= 0 = x
    | otherwise = nTimes (n - 1) f $ f x

when' :: (Monoid p) => Bool -> p -> p
when' b m = if b then m else mempty

whenM' :: (Monad m, Monoid (m b)) => m Bool -> m b -> m b
whenM' mb m = mb >>= flip when' m

modifyM :: (MonadState a m) => (a -> m a) -> m ()
modifyM f = get >>= f >>= put

fst3 :: (a, b, c) -> a
fst3 (x, _, _) = x

snd3 :: (a, b, c) -> b
snd3 (_, y, _) = y

trd3 :: (a, b, c) -> c
trd3 (_, _, z) = z

first3 :: (t -> a) -> (t, b, c) -> (a, b, c)
first3 f (x, y, z) = (f x, y, z)

second3 :: (t -> b) -> (a, t, c) -> (a, b, c)
second3 f (x, y, z) = (x, f y, z)

third3 :: (t -> c) -> (a, b, t) -> (a, b, c)
third3 f (x, y, z) = (x, y, f z)

uncurry3 :: (t1 -> t2 -> t3 -> t4) -> (t1, t2, t3) -> t4
uncurry3 f (x, y, z) = f x y z

curry3 :: ((a, b, c) -> t) -> a -> b -> c -> t
curry3 f x y z = f (x, y, z)

maybeToEnum :: (Ord a, Num a) => a -> Maybe a
maybeToEnum n
    | n < 0 = Nothing
    | otherwise = pure n
