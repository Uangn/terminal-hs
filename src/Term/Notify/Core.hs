module Term.Notify.Core (
    notifyChange,
) where

import Control.Monad.Trans.RWS (RWST (..))
import qualified Data.List as L
import qualified Data.Text as T
import qualified Data.Vector as V

import Parser.Core
import Term.Notify.Parser

import qualified Data.Map.Strict as M
import Term.App.Core
import Term.Prelude
import Term.Types

notifyRunCommand :: (Monoid w) => NotifyCommand -> RWST e w ShellData (MaybeT IO) [String]
notifyRunCommand c = do
    len <- gets $ length . output
    let tellMax = "Error: Maximum is " <> show len
    case c of
        NotifyExit -> empty
        NotifyError s -> pure s
        NotifyLine n ->
            gets
                $ maybe ("Error: Number too large" : [tellMax]) ((: []) . T.unpack)
                . (V.!? fromEnum n)
                . output
        NotifyLineRange n m
            | m < n -> pure ("Error: Bottom of range is larger than top" : [tellMax])
            | fromEnum n > len -> pure $ "Error: Either one or both of the numbers are out of the range" : [tellMax]
            | otherwise ->
                gets
                    $ V.toList
                    . fmap T.unpack
                    . V.reverse
                    . (V.take (fromEnum m + 1) . V.drop (fromEnum n))
                    . output

notifyParseCommand :: String -> NotifyCommand
notifyParseCommand =
    either (NotifyError . L.lines) fst
        . fromMaybe (Left "Error: undefined error")
        . runParserT notifyCommand

notifyChange :: (Monoid w) => ShellEvents -> RWST e w ShellData (MaybeT IO) (Map Int ShellEvents)
notifyChange events = do
    scrollChange
    deleteChange
    exitChange
    handleTextInputs notifyParseCommand notifyRunCommand notifyAddInput events
    pure M.empty

notifyAddInput :: ShellData -> String -> V.Vector Text -> V.Vector Text
notifyAddInput _ _ = id
