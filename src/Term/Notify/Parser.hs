module Term.Notify.Parser (
    notifyCommand,
) where

import Parser.Core
import Parser.List

import Term.Prelude
import Term.Types

import qualified Relude.Unsafe as Unsafe

notifyCommand :: (MonadPlus m, MonadFail m) => ParserT String String m NotifyCommand
notifyCommand =
    join (label "Error: Command not found" $ asum commands)
        <* label "Error: Command not parsed" (spaces *> eof)
  where
    commands =
        (\(ident, rest) -> list ident $> rest)
            <$> [ ("exit", exit)
                , ("", lineRange <|> line)
                ]

nonZeroDigit :: (MonadPlus m) => ParserT String String m Char
nonZeroDigit = mfilter (/= '0') digit

nat :: (MonadPlus m, MonadFail m) => ParserT String String m Natural
nat = label "Invalid natural" num'
  where
    num' =
        (toEnum @Natural)
            . subtract 1
            . Unsafe.read
            <$> liftA2 (:) nonZeroDigit (many digit)

line :: (MonadPlus m, MonadFail m) => ParserT String String m NotifyCommand
line =
    label "Error: unable to parse line number"
        $ fmap NotifyLine nat

lineRange :: (MonadPlus m, MonadFail m) => ParserT String String m NotifyCommand
lineRange =
    label "Error: unable to parse line number range"
        $ liftA2
            NotifyLineRange
            (nat <* comma)
            nat
  where
    comma = label "Error: Expected a comma" $ single ','

exit :: (Monad m) => ParserT e s m NotifyCommand
exit = pure NotifyExit
