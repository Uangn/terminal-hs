module Data.List.Zipper (
    Zipper (..),
    -- fromList,
    toList,
    left,
    right,
    lf,
    rg,
    shiftLeft,
    shiftRight,
    addLeft,
    addRight,
    addLefts,
    addRights,
    tryShiftLeft,
    tryShiftRight,
) where

import Relude hiding (first, fromList, second, toList)

data Zipper a
    = Zipper
    { zipperLeft :: [a]
    , zipperRight :: [a]
    }
    deriving (Show)

instance Semigroup (Zipper a) where
    (Zipper l1 r1) <> (Zipper l2 r2) =
        Zipper (l1 <> r1) (l2 <> r2)

instance Monoid (Zipper a) where
    mempty = Zipper mempty mempty

toList :: Zipper a -> [a]
toList (Zipper l r) = reverse l <> r

-- fromList :: [a] -> Zipper a
-- fromList = undefined

addLeft :: a -> Zipper a -> Zipper a
addLeft x (Zipper l r) = Zipper (x : l) r

addRight :: a -> Zipper a -> Zipper a
addRight x (Zipper l r) = Zipper l (x : r)

addLefts :: [a] -> Zipper a -> Zipper a
addLefts xs = left (reverse xs <>)

addRights :: [a] -> Zipper a -> Zipper a
addRights xs = right (reverse xs <>)

left :: ([a] -> [a]) -> Zipper a -> Zipper a
left f (Zipper l r) = Zipper (f l) r

right :: ([a] -> [a]) -> Zipper a -> Zipper a
right f (Zipper l r) = Zipper l (f r)

lf :: Zipper a -> [a]
lf (Zipper l _) = l

rg :: Zipper a -> [a]
rg (Zipper _ r) = r

shiftLeft :: Zipper a -> Maybe (Zipper a)
shiftLeft (Zipper (l : ls) rs) = pure $ Zipper ls (l : rs)
shiftLeft _ = mempty

shiftRight :: Zipper a -> Maybe (Zipper a)
shiftRight (Zipper ls (r : rs)) = pure $ Zipper (r : ls) rs
shiftRight _ = mempty

tryShiftLeft :: Zipper a -> Zipper a
tryShiftLeft = fromMaybe <*> shiftLeft

tryShiftRight :: Zipper a -> Zipper a
tryShiftRight = fromMaybe <*> shiftRight
